CREATE TABLE blogs (
  blogid serial NOT NULL,
  url character varying(255) NOT NULL,
  name character varying(255) NOT NULL,
  pass character varying(255) NOT NULL,
  remote_id character varying(255) NOT NULL,
  type character varying(255) NOT NULL,
  uid integer NOT NULL,
  title character varying NOT NULL,
  endpoint character varying NOT NULL
);